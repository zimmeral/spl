grammar SPL;

//// lexic

// keywords
TRUE: 'true' ;
FALSE: 'false' ;
AND: 'and' ;
OR: 'or' ;
VAR: 'var' ;
PRINT: 'print' ;
IF: 'if' ;
ELSE: 'else' ;
WHILE: 'while' ;

// special chars
SEMICOL: ';' ;
LPAREN: '(' ;
RPAREN: ')' ;
LBRACE: '{' ;
RBRACE: '}' ;

// value types
NUMBER: [0-9]+('.'[0-9]+)? ;
STRING: '"' ( ~["\r\n] )* '"' ;

// misc
ID: [A-Za-z][A-Za-z0-9]* ;

// ops
ADD: '+' ;
SUB: '-' ;
MULT: '*' ;
DIV: '/' ;
EQ: '=' ;
BEQ: '==' ;
NEQ: '!=' ;
GRT: '>' ;
LSS: '<' ;
GEQ: '>=' ;
LEQ: '<=' ;
NOT: '!' ;

// comments
COMMENT: '//' ~[\r\n]* '\n' -> skip ;

// whites
WS: [\r\t\n ] -> skip ;

ERR: . {print(f"Syntax error at line {self.line}")};

//// syntax

program: declaration* EOF ;

declaration: varDecl | statement ;

varDecl: VAR ID ( EQ ex=expression )? SEMICOL ;

statement: exprStmt | ifStmt | printStmt | whileStmt | block ;

exprStmt: expression SEMICOL ;

ifStmt: IF LPAREN ex=expression RPAREN l1=statement ( ELSE l2=statement )? ;

printStmt: PRINT ex=expression SEMICOL ;

whileStmt: WHILE LPAREN ex=expression RPAREN st=statement ;

block: LBRACE declaration* RBRACE ;

expression: ass=assignment ;

assignment: ID EQ ass=assignment | l1=logic_or ;

logic_or: logic_and ( OR logic_and )* ;
logic_and: equality ( AND equality )* ;

equality: equality_neq | equality_beq ;
equality_neq: comparison ( NEQ comparison )* ;
equality_beq: comparison ( BEQ comparison )* ;

comparison: comp_grt | comp_geq | comp_lss | comp_leq ;
comp_grt: term ( GRT term )* ;
comp_geq: term ( GEQ term )* ;
comp_lss: term ( LSS term )* ;
comp_leq: term ( LEQ term )* ;

term: term_add | term_sub ;
term_add: factor ( ADD factor )* ;
term_sub: factor ( SUB factor )* ;

factor: factor_div | factor_mult ;
factor_div: unary ( DIV unary )* ;
factor_mult: unary ( MULT unary )* ;

unary: (NOT|SUB) unary | primary ;

primary: TRUE | FALSE | NUMBER | STRING | LPAREN ex=expression RPAREN | ID ;
