**Contributer:** markusde,zimmeral

# How to use
Die gewählte Zielsprache ist Python3.

Man fürht den Interpreter so aus:

`python spl.py <FILE>`

# Entwicklungsstand:
Vollständig bearbeitet: **1**, **2.a**

Teilweise bearbeitet: **2.b**, **2.c**

Was aus **2.c** wie funktioniert:
- die Semantik wurde dynamisch mit dem Visitor-Pattern geprüft
- Auswertung aller einstelligen Expressions (längere werden nur mit Klammerung korrekt ausgewertet)
- **print**
- **varDecl**
- **ifStmt**

Was absolut nicht funktioniert:
- Scoping
- **whileStmt**, **assignment**

## Einige Tests
Am besten einzelne Schnipsel testen.

Läuft: `print 1+2`

Läuft nicht: `print (1+2)`

Läuft: `if(true) print 1; else print 2;`
