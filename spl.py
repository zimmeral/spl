import sys
from antlr4 import *
from SPLLexer import SPLLexer
from SPLParser import SPLParser
#from SPLListener import SPLListener
from TestVisitor import TestVisitor
 
def main(argv):
    input_stream = FileStream(argv[1])
    lexer = SPLLexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = SPLParser(stream)
    tree = parser.program()
    # regitrieren unseren eigenen Visitor
    visitor = TestVisitor()
    visitor.visit(tree)
                          
if __name__ == '__main__':
    main(sys.argv)
