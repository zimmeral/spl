#from antlr4 import *
from SPLParser import SPLParser
from SPLVisitor import SPLVisitor

from enum import StrEnum
from functools import reduce

# Speichert die Typen der Werte von SPL
class Types(StrEnum):
    STRING = "string"
    BOOL = "bool"
    NUMBER = "number"

# Implementiert den Interpreter
class TestVisitor(SPLVisitor):
    def __init__(self):
        # environment
        self.vars = {}

    def visitWhileStmt(self, ctx:SPLParser.WhileStmtContext):
        pass

    # Wertet die Kondition 'ex' aus und fuehrt die Anweisung 'l1' aus
    def visitIfStmt(self, ctx:SPLParser.IfStmtContext):
        typ,val = expr = self.visitChildren(ctx.ex)
        if not typ==Types.BOOL:
            print(f"[Line:{ctx.start.line}] if condition must be of type bool.")
            exit(-1)
        if val:
            self.visitChildren(ctx.l1)
        else:
            self.visitChildren(ctx.l2)

    def visitPrintStmt(self, ctx:SPLParser.PrintStmtContext):
        # eval and print expression
        expr = self.visitChildren(ctx.ex)
        if not expr==None:
            print(expr[1])

    def visitVarDecl(self, ctx:SPLParser.VarDeclContext):
        expr = self.visitChildren(ctx.ex)
        # update variables
        varname=ctx.ID().getText()
        if varname in self.vars:
            print(f"[Line:{ctx.start.line}] Redefinition of variable ({varname}) not allowed.")
            exit(-1)
        # declaration only
        if expr==None:
            self.vars[varname] = (None,None) # (Type,Value)
            return
        # definition
        self.vars[varname] = (expr[0],expr[1])
        #print(self.vars)

    def visitExpression(self, ctx:SPLParser.ExpressionContext):
        #print("Expression childs:",ctx.getChildCount())
        childs = self.visitChildren(ctx)
        #print(childs)
        return childs

    # sehr buggy
    def visitAssignment(self, ctx:SPLParser.AssignmentContext):
        l1=self.visitLogic_or(ctx)
        if not l1==None:
            return l1
        # update variable 
        ass = self.visitAssignment(ctx)
        varname=ctx.ID().getText()
        if not varname in self.vars:
            print(f"[Line:{ctx.start.line}] Undefined variable ({varname}).")
            exit(-1)
        self.vars[varname][1]=ass
        return ass

    # Sammle alle Kinder und verknuepfe sie mit OR
    # Die Operaden muessen Boolean sein
    def visitLogic_or(self, ctx:SPLParser.Logic_orContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        res=False
        for c in ctx.getChildren():
            expr=self.visitChildren(c)
            if expr==None:
                continue
            typ,val=expr
            if not typ==Types.BOOL:
                print(f"[Line:{ctx.start.line}] Unsupported operation on type (or,{typ})")
                exit(-1)
            res=res or val
        #print(typ,res)
        return (typ,res)

    # Sammle alle Kinder und verknuepfe sie mit AND
    # Die Operaden muessen Boolean sein
    def visitLogic_and(self, ctx:SPLParser.Logic_andContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        res=True
        for c in ctx.getChildren():
            expr=self.visitChildren(c)
            if expr==None:
                continue
            typ,val=expr
            if not typ==Types.BOOL:
                print(f"[Line:{ctx.start.line}] Unsupported operation on type (and,{typ})")
                exit(-1)
            res=res and val
        #print(typ,res)
        return (typ,res)

    # Sammle alle Kinder und verknuepfe sie mit !=
    # Die Operaden muessen vom selben Typ sein
    def visitEquality_neq(self, ctx:SPLParser.Equality_neqContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        childs=[self.visitChildren(c) for c in ctx.getChildren()]
        childs=list(filter(None,childs))
        c1=childs[0]
        if not all(c1[0]==c[0] for c in childs):
            print(f"[Line:{ctx.start.line}] comparisons must be of the same type.")
            exit(-1)
        res=reduce(lambda a,b: not a==b, (c[1] for c in childs))
        return (c1[0],res)
        

    # Sammle alle Kinder und verknuepfe sie mit ==
    # Die Operaden muessen vom selben Typ sein
    def visitEquality_beq(self, ctx:SPLParser.Equality_beqContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        childs=[self.visitChildren(c) for c in ctx.getChildren()]
        childs=list(filter(None,childs))
        c1=childs[0]
        if not all(c1[0]==c[0] for c in childs):
            print(f"[Line:{ctx.start.line}] comparisons must be of the same type.")
            exit(-1)
        res=reduce(lambda a,b: a==b, (c[1] for c in childs))
        print(res)
        return (c1[0],res)

    # Sammle alle Kinder und verknuepfe sie mit >
    # Das Ergebnis ist ein Boolean
    # Die Operaden muessen vom Typ Number sein
    def visitComp_grt(self, ctx:SPLParser.Comp_grtContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        childs=[self.visitChildren(c) for c in ctx.getChildren()]
        childs=list(filter(None,childs))
        c1=childs[0]
        if not all(c[0]==Types.NUMBER for c in childs):
            print(f"[Line:{ctx.start.line}] artihmetic comparisons must be numbers.")
            exit(-1)
        res=reduce(lambda a,b: a>b, (c[1] for c in childs))
        #print(res)
        return (c1[0],res)

    # Sammle alle Kinder und verknuepfe sie mit >=
    # Das Ergebnis ist ein Boolean
    # Die Operaden muessen vom Typ Number sein
    def visitComp_geq(self, ctx:SPLParser.Comp_geqContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        childs=[self.visitChildren(c) for c in ctx.getChildren()]
        childs=list(filter(None,childs))
        c1=childs[0]
        if not all(c[0]==Types.NUMBER for c in childs):
            print(f"[Line:{ctx.start.line}] artihmetic comparisons must be numbers.")
            exit(-1)
        res=reduce(lambda a,b: a>=b, (c[1] for c in childs))
        #print(res)
        return (c1[0],res)

    # Sammle alle Kinder und verknuepfe sie mit <
    # Das Ergebnis ist ein Boolean
    # Die Operaden muessen vom Typ Number sein
    def visitComp_lss(self, ctx:SPLParser.Comp_lssContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        childs=[self.visitChildren(c) for c in ctx.getChildren()]
        childs=list(filter(None,childs))
        c1=childs[0]
        if not all(c[0]==Types.NUMBER for c in childs):
            print(f"[Line:{ctx.start.line}] artihmetic comparisons must be numbers.")
            exit(-1)
        res=reduce(lambda a,b: a<b, (c[1] for c in childs))
        #print(res)
        return (c1[0],res)

    # Sammle alle Kinder und verknuepfe sie mit >
    # Das Ergebnis ist ein Boolean
    # Die Operaden muessen vom Typ Number sein
    def visitComp_leq(self, ctx:SPLParser.Comp_leqContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        childs=[self.visitChildren(c) for c in ctx.getChildren()]
        childs=list(filter(None,childs))
        c1=childs[0]
        if not all(c[0]==Types.NUMBER for c in childs):
            print(f"[Line:{ctx.start.line}] comparisons only support numbers.")
            exit(-1)
        res=reduce(lambda a,b: a<=b, (c[1] for c in childs))
        #print(res)
        return (c1[0],res)

    # Sammle alle Kinder und verknuepfe sie mit +
    # Das Ergebnis ist eine Number
    # Die Operaden muessen vom Typ Number sein
    def visitTerm_add(self, ctx:SPLParser.Term_addContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        childs=[self.visitChildren(c) for c in ctx.getChildren()]
        childs=list(filter(None,childs))
        c1=childs[0]
        if not all(c[0]==Types.NUMBER for c in childs):
            print(f"[Line:{ctx.start.line}] artihmetic only support numbers.")
            exit(-1)
        res=reduce(lambda a,b: a+b, (c[1] for c in childs))
        #print(res)
        return (c1[0],res)
    
    # Sammle alle Kinder und verknuepfe sie mit -
    # Das Ergebnis ist eine Number
    # Die Operaden muessen vom Typ Number sein
    def visitTerm_sub(self, ctx:SPLParser.Term_subContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        childs=[self.visitChildren(c) for c in ctx.getChildren()]
        childs=list(filter(None,childs))
        c1=childs[0]
        if not all(c[0]==Types.NUMBER for c in childs):
            print(f"[Line:{ctx.start.line}] artihmetic only support numbers.")
            exit(-1)
        res=reduce(lambda a,b: a-b, (c[1] for c in childs))
        #print(res)
        return (c1[0],res)

    # Sammle alle Kinder und verknuepfe sie mit /
    # Das Ergebnis ist eine Number
    # Die Operaden muessen vom Typ Number sein
    def visitFactor_div(self, ctx:SPLParser.Factor_divContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        childs=[self.visitChildren(c) for c in ctx.getChildren()]
        childs=list(filter(None,childs))
        c1=childs[0]
        if not all(c[0]==Types.NUMBER for c in childs):
            print(f"[Line:{ctx.start.line}] artihmetic only support numbers.")
            exit(-1)
        res=reduce(lambda a,b: a/b, (c[1] for c in childs))
        #print(res)
        return (c1[0],res)

    # Sammle alle Kinder und verknuepfe sie mit *
    # Das Ergebnis ist eine Number
    # Die Operaden muessen vom Typ Number sein
    def visitFactor_mult(self, ctx:SPLParser.Factor_multContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        childs=[self.visitChildren(c) for c in ctx.getChildren()]
        childs=list(filter(None,childs))
        c1=childs[0]
        if not all(c[0]==Types.NUMBER for c in childs):
            print(f"[Line:{ctx.start.line}] artihmetic only support numbers.")
            exit(-1)
        res=reduce(lambda a,b: a*b, (c[1] for c in childs))
        #print(res)
        return (c1[0],res)

    # Ivertiere Booleans und Negiere Numbers
    def visitUnary(self, ctx:SPLParser.UnaryContext):
        if(ctx.getChildCount()==1):
            return self.visitChildren(ctx)

        # evaluate unary operator
        unary = self.visitChildren(ctx)
        op = ctx.NOT() or ctx.SUB()
        typ,val = unary
        #print(typ,val,op)
        #print(Types.BOOL)
        #print(ctx.NOT(), ctx.SUB())
        res=unary
        if(typ==Types.NUMBER):
            if(not ctx.SUB()==None):
                res=(typ,-val)
            else:
                print(f"[Line:{ctx.start.line}] Unsupported operation on type ({op},{typ})")
                exit(-1)
        elif(typ==Types.BOOL):
            if(not ctx.NOT()==None):
                res=(typ,not val)
            else:
                print(f"[Line:{ctx.start.line}] Unsupported operation on type ({op},{typ})")
                exit(-1)
        else:
            print(f"[Line:{ctx.start.line}] Unsupported operation on type ({op},{typ})")
            exit(-1)
        #print(res)
        return res

    # Interpretiere die Primitiva
    def visitPrimary(self, ctx:SPLParser.PrimaryContext):
        # visit nested expression
        nested = self.visitExpression(ctx)
        if(nested):
            return nested

        # interpret primitives
        if(ctx.TRUE()):
            return ("bool",True)
        if(ctx.FALSE()):
            return ("bool",False)
        if(ctx.NUMBER()):
            return ("number",float(ctx.getText()))
        if(ctx.STRING()):
            return ("string",ctx.getText()[1:-1])
        if(ctx.ID()):
            return ("id",ctx.getText())
