# Definition der Semantik

Die Semantik wird im Visitor **TestVisitor.py**
umgesetzt. Das **Environment** besteht aus einem Class-Member der in einem Hash-Table die Variablen-Deklarationen verwaltet.

Werte werden als Tuple aus (Typ,Wert) gespeichert.

## Implizite Boolean-Konvertierung
- Ist nicht erlaubt und erzeugt einen Fehler

### Umsetzung
- Conditions testen explizit auf den Typ der Expression.

## Operator-Überladen
Alle unterstützten Kombinationen sind hier aufgelistet. Sie haben die übliche Bedeutung, wie in den meisten Sprachen.
| Operand | Operator | Operand| Bedeutung |
| --- | --- | --- |-|
| String | == | String | String-Vergleich zu Boolean
| String | != | String | String-Vergleich zu Boolean
| Number | + | Number | Standard Float-Arithmetik
| Number | - | Number | ~
| Number | * | Number | ~
| Number | / | Number | ~
| Number | == | Number | Float-Vergleich zu Boolean
| Number | != | Number | Float-Vergleich zu Boolean
| Number | > | Number | Float-Vergleich zu Boolean
| Number | < | Number | Float-Vergleich zu Boolean
| Number | >= | Number | Float-Vergleich zu Boolean
| Number | <= | Number | Float-Vergleich zu Boolean
|  | - | Number | Negierung einer Float Zahl
| Boolean | == | Boolean | Float-Vergleich zu Boolean
| Boolean | != | Boolean |Float-Vergleich zu Boolean
|  | ! | Boolean | Negierung eines Boolean

### Umsetzung
- in den entsprechenden **visit*** Methoden wird der Typ der Operaden geprüft und ggf. gemeldet.

## Neudefinition von Variablen
- Sind nicht erlaubt und erzeugen einen Fehler

### Umsetzung
- in **visitVarDecl()** wird vor dem Anlegen einer Variable auf vorhandene Deklaration in **self.vars** geprüft und ggf. ein Fehler ausgegeben

## Shadowing und Scoping
- Variablen dürfen in inneren Blöcken nicht neu definiert werden
-> also existiert auch kein Shadowing
- Scoping wurde gar nicht umgesetzt
-> Variablen haben den File Scope

## Uninitialisierte Werte
- uninitialisierte Variablen sind so in der Umgebung als solche ohne Wert markiert und führen bei Benutzung zu einem Fehler

### Umsetzung
- keine bis auf Verwaltung der Variablen
