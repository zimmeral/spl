# Generated from SPL.g4 by ANTLR 4.13.0
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO

def serializedATN():
    return [
        4,1,32,255,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
        6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,2,13,7,13,
        2,14,7,14,2,15,7,15,2,16,7,16,2,17,7,17,2,18,7,18,2,19,7,19,2,20,
        7,20,2,21,7,21,2,22,7,22,2,23,7,23,2,24,7,24,2,25,7,25,2,26,7,26,
        2,27,7,27,2,28,7,28,1,0,5,0,60,8,0,10,0,12,0,63,9,0,1,0,1,0,1,1,
        1,1,3,1,69,8,1,1,2,1,2,1,2,1,2,3,2,75,8,2,1,2,1,2,1,3,1,3,1,3,1,
        3,1,3,3,3,84,8,3,1,4,1,4,1,4,1,5,1,5,1,5,1,5,1,5,1,5,1,5,3,5,96,
        8,5,1,6,1,6,1,6,1,6,1,7,1,7,1,7,1,7,1,7,1,7,1,8,1,8,5,8,110,8,8,
        10,8,12,8,113,9,8,1,8,1,8,1,9,1,9,1,10,1,10,1,10,1,10,3,10,123,8,
        10,1,11,1,11,1,11,5,11,128,8,11,10,11,12,11,131,9,11,1,12,1,12,1,
        12,5,12,136,8,12,10,12,12,12,139,9,12,1,13,1,13,3,13,143,8,13,1,
        14,1,14,1,14,5,14,148,8,14,10,14,12,14,151,9,14,1,15,1,15,1,15,5,
        15,156,8,15,10,15,12,15,159,9,15,1,16,1,16,1,16,1,16,3,16,165,8,
        16,1,17,1,17,1,17,5,17,170,8,17,10,17,12,17,173,9,17,1,18,1,18,1,
        18,5,18,178,8,18,10,18,12,18,181,9,18,1,19,1,19,1,19,5,19,186,8,
        19,10,19,12,19,189,9,19,1,20,1,20,1,20,5,20,194,8,20,10,20,12,20,
        197,9,20,1,21,1,21,3,21,201,8,21,1,22,1,22,1,22,5,22,206,8,22,10,
        22,12,22,209,9,22,1,23,1,23,1,23,5,23,214,8,23,10,23,12,23,217,9,
        23,1,24,1,24,3,24,221,8,24,1,25,1,25,1,25,5,25,226,8,25,10,25,12,
        25,229,9,25,1,26,1,26,1,26,5,26,234,8,26,10,26,12,26,237,9,26,1,
        27,1,27,1,27,3,27,242,8,27,1,28,1,28,1,28,1,28,1,28,1,28,1,28,1,
        28,1,28,3,28,253,8,28,1,28,0,0,29,0,2,4,6,8,10,12,14,16,18,20,22,
        24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,0,1,2,0,19,19,
        29,29,259,0,61,1,0,0,0,2,68,1,0,0,0,4,70,1,0,0,0,6,83,1,0,0,0,8,
        85,1,0,0,0,10,88,1,0,0,0,12,97,1,0,0,0,14,101,1,0,0,0,16,107,1,0,
        0,0,18,116,1,0,0,0,20,122,1,0,0,0,22,124,1,0,0,0,24,132,1,0,0,0,
        26,142,1,0,0,0,28,144,1,0,0,0,30,152,1,0,0,0,32,164,1,0,0,0,34,166,
        1,0,0,0,36,174,1,0,0,0,38,182,1,0,0,0,40,190,1,0,0,0,42,200,1,0,
        0,0,44,202,1,0,0,0,46,210,1,0,0,0,48,220,1,0,0,0,50,222,1,0,0,0,
        52,230,1,0,0,0,54,241,1,0,0,0,56,252,1,0,0,0,58,60,3,2,1,0,59,58,
        1,0,0,0,60,63,1,0,0,0,61,59,1,0,0,0,61,62,1,0,0,0,62,64,1,0,0,0,
        63,61,1,0,0,0,64,65,5,0,0,1,65,1,1,0,0,0,66,69,3,4,2,0,67,69,3,6,
        3,0,68,66,1,0,0,0,68,67,1,0,0,0,69,3,1,0,0,0,70,71,5,5,0,0,71,74,
        5,17,0,0,72,73,5,22,0,0,73,75,3,18,9,0,74,72,1,0,0,0,74,75,1,0,0,
        0,75,76,1,0,0,0,76,77,5,10,0,0,77,5,1,0,0,0,78,84,3,8,4,0,79,84,
        3,10,5,0,80,84,3,12,6,0,81,84,3,14,7,0,82,84,3,16,8,0,83,78,1,0,
        0,0,83,79,1,0,0,0,83,80,1,0,0,0,83,81,1,0,0,0,83,82,1,0,0,0,84,7,
        1,0,0,0,85,86,3,18,9,0,86,87,5,10,0,0,87,9,1,0,0,0,88,89,5,7,0,0,
        89,90,5,11,0,0,90,91,3,18,9,0,91,92,5,12,0,0,92,95,3,6,3,0,93,94,
        5,8,0,0,94,96,3,6,3,0,95,93,1,0,0,0,95,96,1,0,0,0,96,11,1,0,0,0,
        97,98,5,6,0,0,98,99,3,18,9,0,99,100,5,10,0,0,100,13,1,0,0,0,101,
        102,5,9,0,0,102,103,5,11,0,0,103,104,3,18,9,0,104,105,5,12,0,0,105,
        106,3,6,3,0,106,15,1,0,0,0,107,111,5,13,0,0,108,110,3,2,1,0,109,
        108,1,0,0,0,110,113,1,0,0,0,111,109,1,0,0,0,111,112,1,0,0,0,112,
        114,1,0,0,0,113,111,1,0,0,0,114,115,5,14,0,0,115,17,1,0,0,0,116,
        117,3,20,10,0,117,19,1,0,0,0,118,119,5,17,0,0,119,120,5,22,0,0,120,
        123,3,20,10,0,121,123,3,22,11,0,122,118,1,0,0,0,122,121,1,0,0,0,
        123,21,1,0,0,0,124,129,3,24,12,0,125,126,5,4,0,0,126,128,3,24,12,
        0,127,125,1,0,0,0,128,131,1,0,0,0,129,127,1,0,0,0,129,130,1,0,0,
        0,130,23,1,0,0,0,131,129,1,0,0,0,132,137,3,26,13,0,133,134,5,3,0,
        0,134,136,3,26,13,0,135,133,1,0,0,0,136,139,1,0,0,0,137,135,1,0,
        0,0,137,138,1,0,0,0,138,25,1,0,0,0,139,137,1,0,0,0,140,143,3,28,
        14,0,141,143,3,30,15,0,142,140,1,0,0,0,142,141,1,0,0,0,143,27,1,
        0,0,0,144,149,3,32,16,0,145,146,5,24,0,0,146,148,3,32,16,0,147,145,
        1,0,0,0,148,151,1,0,0,0,149,147,1,0,0,0,149,150,1,0,0,0,150,29,1,
        0,0,0,151,149,1,0,0,0,152,157,3,32,16,0,153,154,5,23,0,0,154,156,
        3,32,16,0,155,153,1,0,0,0,156,159,1,0,0,0,157,155,1,0,0,0,157,158,
        1,0,0,0,158,31,1,0,0,0,159,157,1,0,0,0,160,165,3,34,17,0,161,165,
        3,36,18,0,162,165,3,38,19,0,163,165,3,40,20,0,164,160,1,0,0,0,164,
        161,1,0,0,0,164,162,1,0,0,0,164,163,1,0,0,0,165,33,1,0,0,0,166,171,
        3,42,21,0,167,168,5,25,0,0,168,170,3,42,21,0,169,167,1,0,0,0,170,
        173,1,0,0,0,171,169,1,0,0,0,171,172,1,0,0,0,172,35,1,0,0,0,173,171,
        1,0,0,0,174,179,3,42,21,0,175,176,5,27,0,0,176,178,3,42,21,0,177,
        175,1,0,0,0,178,181,1,0,0,0,179,177,1,0,0,0,179,180,1,0,0,0,180,
        37,1,0,0,0,181,179,1,0,0,0,182,187,3,42,21,0,183,184,5,26,0,0,184,
        186,3,42,21,0,185,183,1,0,0,0,186,189,1,0,0,0,187,185,1,0,0,0,187,
        188,1,0,0,0,188,39,1,0,0,0,189,187,1,0,0,0,190,195,3,42,21,0,191,
        192,5,28,0,0,192,194,3,42,21,0,193,191,1,0,0,0,194,197,1,0,0,0,195,
        193,1,0,0,0,195,196,1,0,0,0,196,41,1,0,0,0,197,195,1,0,0,0,198,201,
        3,44,22,0,199,201,3,46,23,0,200,198,1,0,0,0,200,199,1,0,0,0,201,
        43,1,0,0,0,202,207,3,48,24,0,203,204,5,18,0,0,204,206,3,48,24,0,
        205,203,1,0,0,0,206,209,1,0,0,0,207,205,1,0,0,0,207,208,1,0,0,0,
        208,45,1,0,0,0,209,207,1,0,0,0,210,215,3,48,24,0,211,212,5,19,0,
        0,212,214,3,48,24,0,213,211,1,0,0,0,214,217,1,0,0,0,215,213,1,0,
        0,0,215,216,1,0,0,0,216,47,1,0,0,0,217,215,1,0,0,0,218,221,3,50,
        25,0,219,221,3,52,26,0,220,218,1,0,0,0,220,219,1,0,0,0,221,49,1,
        0,0,0,222,227,3,54,27,0,223,224,5,21,0,0,224,226,3,54,27,0,225,223,
        1,0,0,0,226,229,1,0,0,0,227,225,1,0,0,0,227,228,1,0,0,0,228,51,1,
        0,0,0,229,227,1,0,0,0,230,235,3,54,27,0,231,232,5,20,0,0,232,234,
        3,54,27,0,233,231,1,0,0,0,234,237,1,0,0,0,235,233,1,0,0,0,235,236,
        1,0,0,0,236,53,1,0,0,0,237,235,1,0,0,0,238,239,7,0,0,0,239,242,3,
        54,27,0,240,242,3,56,28,0,241,238,1,0,0,0,241,240,1,0,0,0,242,55,
        1,0,0,0,243,253,5,1,0,0,244,253,5,2,0,0,245,253,5,15,0,0,246,253,
        5,16,0,0,247,248,5,11,0,0,248,249,3,18,9,0,249,250,5,12,0,0,250,
        253,1,0,0,0,251,253,5,17,0,0,252,243,1,0,0,0,252,244,1,0,0,0,252,
        245,1,0,0,0,252,246,1,0,0,0,252,247,1,0,0,0,252,251,1,0,0,0,253,
        57,1,0,0,0,25,61,68,74,83,95,111,122,129,137,142,149,157,164,171,
        179,187,195,200,207,215,220,227,235,241,252
    ]

class SPLParser ( Parser ):

    grammarFileName = "SPL.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'true'", "'false'", "'and'", "'or'", 
                     "'var'", "'print'", "'if'", "'else'", "'while'", "';'", 
                     "'('", "')'", "'{'", "'}'", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "'+'", "'-'", "'*'", "'/'", "'='", "'=='", 
                     "'!='", "'>'", "'<'", "'>='", "'<='", "'!'" ]

    symbolicNames = [ "<INVALID>", "TRUE", "FALSE", "AND", "OR", "VAR", 
                      "PRINT", "IF", "ELSE", "WHILE", "SEMICOL", "LPAREN", 
                      "RPAREN", "LBRACE", "RBRACE", "NUMBER", "STRING", 
                      "ID", "ADD", "SUB", "MULT", "DIV", "EQ", "BEQ", "NEQ", 
                      "GRT", "LSS", "GEQ", "LEQ", "NOT", "COMMENT", "WS", 
                      "ERR" ]

    RULE_program = 0
    RULE_declaration = 1
    RULE_varDecl = 2
    RULE_statement = 3
    RULE_exprStmt = 4
    RULE_ifStmt = 5
    RULE_printStmt = 6
    RULE_whileStmt = 7
    RULE_block = 8
    RULE_expression = 9
    RULE_assignment = 10
    RULE_logic_or = 11
    RULE_logic_and = 12
    RULE_equality = 13
    RULE_equality_neq = 14
    RULE_equality_beq = 15
    RULE_comparison = 16
    RULE_comp_grt = 17
    RULE_comp_geq = 18
    RULE_comp_lss = 19
    RULE_comp_leq = 20
    RULE_term = 21
    RULE_term_add = 22
    RULE_term_sub = 23
    RULE_factor = 24
    RULE_factor_div = 25
    RULE_factor_mult = 26
    RULE_unary = 27
    RULE_primary = 28

    ruleNames =  [ "program", "declaration", "varDecl", "statement", "exprStmt", 
                   "ifStmt", "printStmt", "whileStmt", "block", "expression", 
                   "assignment", "logic_or", "logic_and", "equality", "equality_neq", 
                   "equality_beq", "comparison", "comp_grt", "comp_geq", 
                   "comp_lss", "comp_leq", "term", "term_add", "term_sub", 
                   "factor", "factor_div", "factor_mult", "unary", "primary" ]

    EOF = Token.EOF
    TRUE=1
    FALSE=2
    AND=3
    OR=4
    VAR=5
    PRINT=6
    IF=7
    ELSE=8
    WHILE=9
    SEMICOL=10
    LPAREN=11
    RPAREN=12
    LBRACE=13
    RBRACE=14
    NUMBER=15
    STRING=16
    ID=17
    ADD=18
    SUB=19
    MULT=20
    DIV=21
    EQ=22
    BEQ=23
    NEQ=24
    GRT=25
    LSS=26
    GEQ=27
    LEQ=28
    NOT=29
    COMMENT=30
    WS=31
    ERR=32

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.13.0")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(SPLParser.EOF, 0)

        def declaration(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.DeclarationContext)
            else:
                return self.getTypedRuleContext(SPLParser.DeclarationContext,i)


        def getRuleIndex(self):
            return SPLParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = SPLParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 61
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & 537635558) != 0):
                self.state = 58
                self.declaration()
                self.state = 63
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 64
            self.match(SPLParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DeclarationContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varDecl(self):
            return self.getTypedRuleContext(SPLParser.VarDeclContext,0)


        def statement(self):
            return self.getTypedRuleContext(SPLParser.StatementContext,0)


        def getRuleIndex(self):
            return SPLParser.RULE_declaration

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDeclaration" ):
                listener.enterDeclaration(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDeclaration" ):
                listener.exitDeclaration(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDeclaration" ):
                return visitor.visitDeclaration(self)
            else:
                return visitor.visitChildren(self)




    def declaration(self):

        localctx = SPLParser.DeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_declaration)
        try:
            self.state = 68
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [5]:
                self.enterOuterAlt(localctx, 1)
                self.state = 66
                self.varDecl()
                pass
            elif token in [1, 2, 6, 7, 9, 11, 13, 15, 16, 17, 19, 29]:
                self.enterOuterAlt(localctx, 2)
                self.state = 67
                self.statement()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarDeclContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.ex = None # ExpressionContext

        def VAR(self):
            return self.getToken(SPLParser.VAR, 0)

        def ID(self):
            return self.getToken(SPLParser.ID, 0)

        def SEMICOL(self):
            return self.getToken(SPLParser.SEMICOL, 0)

        def EQ(self):
            return self.getToken(SPLParser.EQ, 0)

        def expression(self):
            return self.getTypedRuleContext(SPLParser.ExpressionContext,0)


        def getRuleIndex(self):
            return SPLParser.RULE_varDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVarDecl" ):
                listener.enterVarDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVarDecl" ):
                listener.exitVarDecl(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVarDecl" ):
                return visitor.visitVarDecl(self)
            else:
                return visitor.visitChildren(self)




    def varDecl(self):

        localctx = SPLParser.VarDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_varDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 70
            self.match(SPLParser.VAR)
            self.state = 71
            self.match(SPLParser.ID)
            self.state = 74
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==22:
                self.state = 72
                self.match(SPLParser.EQ)
                self.state = 73
                localctx.ex = self.expression()


            self.state = 76
            self.match(SPLParser.SEMICOL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def exprStmt(self):
            return self.getTypedRuleContext(SPLParser.ExprStmtContext,0)


        def ifStmt(self):
            return self.getTypedRuleContext(SPLParser.IfStmtContext,0)


        def printStmt(self):
            return self.getTypedRuleContext(SPLParser.PrintStmtContext,0)


        def whileStmt(self):
            return self.getTypedRuleContext(SPLParser.WhileStmtContext,0)


        def block(self):
            return self.getTypedRuleContext(SPLParser.BlockContext,0)


        def getRuleIndex(self):
            return SPLParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStatement" ):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = SPLParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_statement)
        try:
            self.state = 83
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [1, 2, 11, 15, 16, 17, 19, 29]:
                self.enterOuterAlt(localctx, 1)
                self.state = 78
                self.exprStmt()
                pass
            elif token in [7]:
                self.enterOuterAlt(localctx, 2)
                self.state = 79
                self.ifStmt()
                pass
            elif token in [6]:
                self.enterOuterAlt(localctx, 3)
                self.state = 80
                self.printStmt()
                pass
            elif token in [9]:
                self.enterOuterAlt(localctx, 4)
                self.state = 81
                self.whileStmt()
                pass
            elif token in [13]:
                self.enterOuterAlt(localctx, 5)
                self.state = 82
                self.block()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprStmtContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(SPLParser.ExpressionContext,0)


        def SEMICOL(self):
            return self.getToken(SPLParser.SEMICOL, 0)

        def getRuleIndex(self):
            return SPLParser.RULE_exprStmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExprStmt" ):
                listener.enterExprStmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExprStmt" ):
                listener.exitExprStmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExprStmt" ):
                return visitor.visitExprStmt(self)
            else:
                return visitor.visitChildren(self)




    def exprStmt(self):

        localctx = SPLParser.ExprStmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_exprStmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 85
            self.expression()
            self.state = 86
            self.match(SPLParser.SEMICOL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IfStmtContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.ex = None # ExpressionContext
            self.l1 = None # StatementContext
            self.l2 = None # StatementContext

        def IF(self):
            return self.getToken(SPLParser.IF, 0)

        def LPAREN(self):
            return self.getToken(SPLParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(SPLParser.RPAREN, 0)

        def expression(self):
            return self.getTypedRuleContext(SPLParser.ExpressionContext,0)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.StatementContext)
            else:
                return self.getTypedRuleContext(SPLParser.StatementContext,i)


        def ELSE(self):
            return self.getToken(SPLParser.ELSE, 0)

        def getRuleIndex(self):
            return SPLParser.RULE_ifStmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIfStmt" ):
                listener.enterIfStmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIfStmt" ):
                listener.exitIfStmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfStmt" ):
                return visitor.visitIfStmt(self)
            else:
                return visitor.visitChildren(self)




    def ifStmt(self):

        localctx = SPLParser.IfStmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_ifStmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 88
            self.match(SPLParser.IF)
            self.state = 89
            self.match(SPLParser.LPAREN)
            self.state = 90
            localctx.ex = self.expression()
            self.state = 91
            self.match(SPLParser.RPAREN)
            self.state = 92
            localctx.l1 = self.statement()
            self.state = 95
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.state = 93
                self.match(SPLParser.ELSE)
                self.state = 94
                localctx.l2 = self.statement()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrintStmtContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.ex = None # ExpressionContext

        def PRINT(self):
            return self.getToken(SPLParser.PRINT, 0)

        def SEMICOL(self):
            return self.getToken(SPLParser.SEMICOL, 0)

        def expression(self):
            return self.getTypedRuleContext(SPLParser.ExpressionContext,0)


        def getRuleIndex(self):
            return SPLParser.RULE_printStmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrintStmt" ):
                listener.enterPrintStmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrintStmt" ):
                listener.exitPrintStmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrintStmt" ):
                return visitor.visitPrintStmt(self)
            else:
                return visitor.visitChildren(self)




    def printStmt(self):

        localctx = SPLParser.PrintStmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_printStmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 97
            self.match(SPLParser.PRINT)
            self.state = 98
            localctx.ex = self.expression()
            self.state = 99
            self.match(SPLParser.SEMICOL)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WhileStmtContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.ex = None # ExpressionContext
            self.st = None # StatementContext

        def WHILE(self):
            return self.getToken(SPLParser.WHILE, 0)

        def LPAREN(self):
            return self.getToken(SPLParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(SPLParser.RPAREN, 0)

        def expression(self):
            return self.getTypedRuleContext(SPLParser.ExpressionContext,0)


        def statement(self):
            return self.getTypedRuleContext(SPLParser.StatementContext,0)


        def getRuleIndex(self):
            return SPLParser.RULE_whileStmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhileStmt" ):
                listener.enterWhileStmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhileStmt" ):
                listener.exitWhileStmt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWhileStmt" ):
                return visitor.visitWhileStmt(self)
            else:
                return visitor.visitChildren(self)




    def whileStmt(self):

        localctx = SPLParser.WhileStmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_whileStmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 101
            self.match(SPLParser.WHILE)
            self.state = 102
            self.match(SPLParser.LPAREN)
            self.state = 103
            localctx.ex = self.expression()
            self.state = 104
            self.match(SPLParser.RPAREN)
            self.state = 105
            localctx.st = self.statement()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BlockContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LBRACE(self):
            return self.getToken(SPLParser.LBRACE, 0)

        def RBRACE(self):
            return self.getToken(SPLParser.RBRACE, 0)

        def declaration(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.DeclarationContext)
            else:
                return self.getTypedRuleContext(SPLParser.DeclarationContext,i)


        def getRuleIndex(self):
            return SPLParser.RULE_block

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBlock" ):
                listener.enterBlock(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBlock" ):
                listener.exitBlock(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlock" ):
                return visitor.visitBlock(self)
            else:
                return visitor.visitChildren(self)




    def block(self):

        localctx = SPLParser.BlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_block)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 107
            self.match(SPLParser.LBRACE)
            self.state = 111
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & 537635558) != 0):
                self.state = 108
                self.declaration()
                self.state = 113
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 114
            self.match(SPLParser.RBRACE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.ass = None # AssignmentContext

        def assignment(self):
            return self.getTypedRuleContext(SPLParser.AssignmentContext,0)


        def getRuleIndex(self):
            return SPLParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression" ):
                return visitor.visitExpression(self)
            else:
                return visitor.visitChildren(self)




    def expression(self):

        localctx = SPLParser.ExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_expression)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 116
            localctx.ass = self.assignment()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AssignmentContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.ass = None # AssignmentContext
            self.l1 = None # Logic_orContext

        def ID(self):
            return self.getToken(SPLParser.ID, 0)

        def EQ(self):
            return self.getToken(SPLParser.EQ, 0)

        def assignment(self):
            return self.getTypedRuleContext(SPLParser.AssignmentContext,0)


        def logic_or(self):
            return self.getTypedRuleContext(SPLParser.Logic_orContext,0)


        def getRuleIndex(self):
            return SPLParser.RULE_assignment

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssignment" ):
                listener.enterAssignment(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssignment" ):
                listener.exitAssignment(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssignment" ):
                return visitor.visitAssignment(self)
            else:
                return visitor.visitChildren(self)




    def assignment(self):

        localctx = SPLParser.AssignmentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_assignment)
        try:
            self.state = 122
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 118
                self.match(SPLParser.ID)
                self.state = 119
                self.match(SPLParser.EQ)
                self.state = 120
                localctx.ass = self.assignment()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 121
                localctx.l1 = self.logic_or()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Logic_orContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def logic_and(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.Logic_andContext)
            else:
                return self.getTypedRuleContext(SPLParser.Logic_andContext,i)


        def OR(self, i:int=None):
            if i is None:
                return self.getTokens(SPLParser.OR)
            else:
                return self.getToken(SPLParser.OR, i)

        def getRuleIndex(self):
            return SPLParser.RULE_logic_or

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLogic_or" ):
                listener.enterLogic_or(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLogic_or" ):
                listener.exitLogic_or(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLogic_or" ):
                return visitor.visitLogic_or(self)
            else:
                return visitor.visitChildren(self)




    def logic_or(self):

        localctx = SPLParser.Logic_orContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_logic_or)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 124
            self.logic_and()
            self.state = 129
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==4:
                self.state = 125
                self.match(SPLParser.OR)
                self.state = 126
                self.logic_and()
                self.state = 131
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Logic_andContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def equality(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.EqualityContext)
            else:
                return self.getTypedRuleContext(SPLParser.EqualityContext,i)


        def AND(self, i:int=None):
            if i is None:
                return self.getTokens(SPLParser.AND)
            else:
                return self.getToken(SPLParser.AND, i)

        def getRuleIndex(self):
            return SPLParser.RULE_logic_and

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLogic_and" ):
                listener.enterLogic_and(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLogic_and" ):
                listener.exitLogic_and(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLogic_and" ):
                return visitor.visitLogic_and(self)
            else:
                return visitor.visitChildren(self)




    def logic_and(self):

        localctx = SPLParser.Logic_andContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_logic_and)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 132
            self.equality()
            self.state = 137
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==3:
                self.state = 133
                self.match(SPLParser.AND)
                self.state = 134
                self.equality()
                self.state = 139
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EqualityContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def equality_neq(self):
            return self.getTypedRuleContext(SPLParser.Equality_neqContext,0)


        def equality_beq(self):
            return self.getTypedRuleContext(SPLParser.Equality_beqContext,0)


        def getRuleIndex(self):
            return SPLParser.RULE_equality

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEquality" ):
                listener.enterEquality(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEquality" ):
                listener.exitEquality(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEquality" ):
                return visitor.visitEquality(self)
            else:
                return visitor.visitChildren(self)




    def equality(self):

        localctx = SPLParser.EqualityContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_equality)
        try:
            self.state = 142
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 140
                self.equality_neq()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 141
                self.equality_beq()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Equality_neqContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def comparison(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.ComparisonContext)
            else:
                return self.getTypedRuleContext(SPLParser.ComparisonContext,i)


        def NEQ(self, i:int=None):
            if i is None:
                return self.getTokens(SPLParser.NEQ)
            else:
                return self.getToken(SPLParser.NEQ, i)

        def getRuleIndex(self):
            return SPLParser.RULE_equality_neq

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEquality_neq" ):
                listener.enterEquality_neq(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEquality_neq" ):
                listener.exitEquality_neq(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEquality_neq" ):
                return visitor.visitEquality_neq(self)
            else:
                return visitor.visitChildren(self)




    def equality_neq(self):

        localctx = SPLParser.Equality_neqContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_equality_neq)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 144
            self.comparison()
            self.state = 149
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==24:
                self.state = 145
                self.match(SPLParser.NEQ)
                self.state = 146
                self.comparison()
                self.state = 151
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Equality_beqContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def comparison(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.ComparisonContext)
            else:
                return self.getTypedRuleContext(SPLParser.ComparisonContext,i)


        def BEQ(self, i:int=None):
            if i is None:
                return self.getTokens(SPLParser.BEQ)
            else:
                return self.getToken(SPLParser.BEQ, i)

        def getRuleIndex(self):
            return SPLParser.RULE_equality_beq

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEquality_beq" ):
                listener.enterEquality_beq(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEquality_beq" ):
                listener.exitEquality_beq(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEquality_beq" ):
                return visitor.visitEquality_beq(self)
            else:
                return visitor.visitChildren(self)




    def equality_beq(self):

        localctx = SPLParser.Equality_beqContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_equality_beq)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 152
            self.comparison()
            self.state = 157
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==23:
                self.state = 153
                self.match(SPLParser.BEQ)
                self.state = 154
                self.comparison()
                self.state = 159
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ComparisonContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def comp_grt(self):
            return self.getTypedRuleContext(SPLParser.Comp_grtContext,0)


        def comp_geq(self):
            return self.getTypedRuleContext(SPLParser.Comp_geqContext,0)


        def comp_lss(self):
            return self.getTypedRuleContext(SPLParser.Comp_lssContext,0)


        def comp_leq(self):
            return self.getTypedRuleContext(SPLParser.Comp_leqContext,0)


        def getRuleIndex(self):
            return SPLParser.RULE_comparison

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComparison" ):
                listener.enterComparison(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComparison" ):
                listener.exitComparison(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComparison" ):
                return visitor.visitComparison(self)
            else:
                return visitor.visitChildren(self)




    def comparison(self):

        localctx = SPLParser.ComparisonContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_comparison)
        try:
            self.state = 164
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 160
                self.comp_grt()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 161
                self.comp_geq()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 162
                self.comp_lss()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 163
                self.comp_leq()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Comp_grtContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.TermContext)
            else:
                return self.getTypedRuleContext(SPLParser.TermContext,i)


        def GRT(self, i:int=None):
            if i is None:
                return self.getTokens(SPLParser.GRT)
            else:
                return self.getToken(SPLParser.GRT, i)

        def getRuleIndex(self):
            return SPLParser.RULE_comp_grt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComp_grt" ):
                listener.enterComp_grt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComp_grt" ):
                listener.exitComp_grt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComp_grt" ):
                return visitor.visitComp_grt(self)
            else:
                return visitor.visitChildren(self)




    def comp_grt(self):

        localctx = SPLParser.Comp_grtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_comp_grt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 166
            self.term()
            self.state = 171
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==25:
                self.state = 167
                self.match(SPLParser.GRT)
                self.state = 168
                self.term()
                self.state = 173
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Comp_geqContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.TermContext)
            else:
                return self.getTypedRuleContext(SPLParser.TermContext,i)


        def GEQ(self, i:int=None):
            if i is None:
                return self.getTokens(SPLParser.GEQ)
            else:
                return self.getToken(SPLParser.GEQ, i)

        def getRuleIndex(self):
            return SPLParser.RULE_comp_geq

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComp_geq" ):
                listener.enterComp_geq(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComp_geq" ):
                listener.exitComp_geq(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComp_geq" ):
                return visitor.visitComp_geq(self)
            else:
                return visitor.visitChildren(self)




    def comp_geq(self):

        localctx = SPLParser.Comp_geqContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_comp_geq)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 174
            self.term()
            self.state = 179
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==27:
                self.state = 175
                self.match(SPLParser.GEQ)
                self.state = 176
                self.term()
                self.state = 181
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Comp_lssContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.TermContext)
            else:
                return self.getTypedRuleContext(SPLParser.TermContext,i)


        def LSS(self, i:int=None):
            if i is None:
                return self.getTokens(SPLParser.LSS)
            else:
                return self.getToken(SPLParser.LSS, i)

        def getRuleIndex(self):
            return SPLParser.RULE_comp_lss

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComp_lss" ):
                listener.enterComp_lss(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComp_lss" ):
                listener.exitComp_lss(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComp_lss" ):
                return visitor.visitComp_lss(self)
            else:
                return visitor.visitChildren(self)




    def comp_lss(self):

        localctx = SPLParser.Comp_lssContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_comp_lss)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 182
            self.term()
            self.state = 187
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==26:
                self.state = 183
                self.match(SPLParser.LSS)
                self.state = 184
                self.term()
                self.state = 189
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Comp_leqContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.TermContext)
            else:
                return self.getTypedRuleContext(SPLParser.TermContext,i)


        def LEQ(self, i:int=None):
            if i is None:
                return self.getTokens(SPLParser.LEQ)
            else:
                return self.getToken(SPLParser.LEQ, i)

        def getRuleIndex(self):
            return SPLParser.RULE_comp_leq

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComp_leq" ):
                listener.enterComp_leq(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComp_leq" ):
                listener.exitComp_leq(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComp_leq" ):
                return visitor.visitComp_leq(self)
            else:
                return visitor.visitChildren(self)




    def comp_leq(self):

        localctx = SPLParser.Comp_leqContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_comp_leq)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 190
            self.term()
            self.state = 195
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==28:
                self.state = 191
                self.match(SPLParser.LEQ)
                self.state = 192
                self.term()
                self.state = 197
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TermContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def term_add(self):
            return self.getTypedRuleContext(SPLParser.Term_addContext,0)


        def term_sub(self):
            return self.getTypedRuleContext(SPLParser.Term_subContext,0)


        def getRuleIndex(self):
            return SPLParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTerm" ):
                return visitor.visitTerm(self)
            else:
                return visitor.visitChildren(self)




    def term(self):

        localctx = SPLParser.TermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_term)
        try:
            self.state = 200
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,17,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 198
                self.term_add()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 199
                self.term_sub()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Term_addContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def factor(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.FactorContext)
            else:
                return self.getTypedRuleContext(SPLParser.FactorContext,i)


        def ADD(self, i:int=None):
            if i is None:
                return self.getTokens(SPLParser.ADD)
            else:
                return self.getToken(SPLParser.ADD, i)

        def getRuleIndex(self):
            return SPLParser.RULE_term_add

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm_add" ):
                listener.enterTerm_add(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm_add" ):
                listener.exitTerm_add(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTerm_add" ):
                return visitor.visitTerm_add(self)
            else:
                return visitor.visitChildren(self)




    def term_add(self):

        localctx = SPLParser.Term_addContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_term_add)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 202
            self.factor()
            self.state = 207
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==18:
                self.state = 203
                self.match(SPLParser.ADD)
                self.state = 204
                self.factor()
                self.state = 209
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Term_subContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def factor(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.FactorContext)
            else:
                return self.getTypedRuleContext(SPLParser.FactorContext,i)


        def SUB(self, i:int=None):
            if i is None:
                return self.getTokens(SPLParser.SUB)
            else:
                return self.getToken(SPLParser.SUB, i)

        def getRuleIndex(self):
            return SPLParser.RULE_term_sub

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm_sub" ):
                listener.enterTerm_sub(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm_sub" ):
                listener.exitTerm_sub(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTerm_sub" ):
                return visitor.visitTerm_sub(self)
            else:
                return visitor.visitChildren(self)




    def term_sub(self):

        localctx = SPLParser.Term_subContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_term_sub)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 210
            self.factor()
            self.state = 215
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==19:
                self.state = 211
                self.match(SPLParser.SUB)
                self.state = 212
                self.factor()
                self.state = 217
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FactorContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def factor_div(self):
            return self.getTypedRuleContext(SPLParser.Factor_divContext,0)


        def factor_mult(self):
            return self.getTypedRuleContext(SPLParser.Factor_multContext,0)


        def getRuleIndex(self):
            return SPLParser.RULE_factor

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactor" ):
                listener.enterFactor(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactor" ):
                listener.exitFactor(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFactor" ):
                return visitor.visitFactor(self)
            else:
                return visitor.visitChildren(self)




    def factor(self):

        localctx = SPLParser.FactorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_factor)
        try:
            self.state = 220
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,20,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 218
                self.factor_div()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 219
                self.factor_mult()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Factor_divContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def unary(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.UnaryContext)
            else:
                return self.getTypedRuleContext(SPLParser.UnaryContext,i)


        def DIV(self, i:int=None):
            if i is None:
                return self.getTokens(SPLParser.DIV)
            else:
                return self.getToken(SPLParser.DIV, i)

        def getRuleIndex(self):
            return SPLParser.RULE_factor_div

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactor_div" ):
                listener.enterFactor_div(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactor_div" ):
                listener.exitFactor_div(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFactor_div" ):
                return visitor.visitFactor_div(self)
            else:
                return visitor.visitChildren(self)




    def factor_div(self):

        localctx = SPLParser.Factor_divContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_factor_div)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 222
            self.unary()
            self.state = 227
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==21:
                self.state = 223
                self.match(SPLParser.DIV)
                self.state = 224
                self.unary()
                self.state = 229
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Factor_multContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def unary(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SPLParser.UnaryContext)
            else:
                return self.getTypedRuleContext(SPLParser.UnaryContext,i)


        def MULT(self, i:int=None):
            if i is None:
                return self.getTokens(SPLParser.MULT)
            else:
                return self.getToken(SPLParser.MULT, i)

        def getRuleIndex(self):
            return SPLParser.RULE_factor_mult

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFactor_mult" ):
                listener.enterFactor_mult(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFactor_mult" ):
                listener.exitFactor_mult(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFactor_mult" ):
                return visitor.visitFactor_mult(self)
            else:
                return visitor.visitChildren(self)




    def factor_mult(self):

        localctx = SPLParser.Factor_multContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_factor_mult)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 230
            self.unary()
            self.state = 235
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==20:
                self.state = 231
                self.match(SPLParser.MULT)
                self.state = 232
                self.unary()
                self.state = 237
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class UnaryContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def unary(self):
            return self.getTypedRuleContext(SPLParser.UnaryContext,0)


        def NOT(self):
            return self.getToken(SPLParser.NOT, 0)

        def SUB(self):
            return self.getToken(SPLParser.SUB, 0)

        def primary(self):
            return self.getTypedRuleContext(SPLParser.PrimaryContext,0)


        def getRuleIndex(self):
            return SPLParser.RULE_unary

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnary" ):
                listener.enterUnary(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnary" ):
                listener.exitUnary(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnary" ):
                return visitor.visitUnary(self)
            else:
                return visitor.visitChildren(self)




    def unary(self):

        localctx = SPLParser.UnaryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_unary)
        self._la = 0 # Token type
        try:
            self.state = 241
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [19, 29]:
                self.enterOuterAlt(localctx, 1)
                self.state = 238
                _la = self._input.LA(1)
                if not(_la==19 or _la==29):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 239
                self.unary()
                pass
            elif token in [1, 2, 11, 15, 16, 17]:
                self.enterOuterAlt(localctx, 2)
                self.state = 240
                self.primary()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrimaryContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.ex = None # ExpressionContext

        def TRUE(self):
            return self.getToken(SPLParser.TRUE, 0)

        def FALSE(self):
            return self.getToken(SPLParser.FALSE, 0)

        def NUMBER(self):
            return self.getToken(SPLParser.NUMBER, 0)

        def STRING(self):
            return self.getToken(SPLParser.STRING, 0)

        def LPAREN(self):
            return self.getToken(SPLParser.LPAREN, 0)

        def RPAREN(self):
            return self.getToken(SPLParser.RPAREN, 0)

        def expression(self):
            return self.getTypedRuleContext(SPLParser.ExpressionContext,0)


        def ID(self):
            return self.getToken(SPLParser.ID, 0)

        def getRuleIndex(self):
            return SPLParser.RULE_primary

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrimary" ):
                listener.enterPrimary(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrimary" ):
                listener.exitPrimary(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrimary" ):
                return visitor.visitPrimary(self)
            else:
                return visitor.visitChildren(self)




    def primary(self):

        localctx = SPLParser.PrimaryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_primary)
        try:
            self.state = 252
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [1]:
                self.enterOuterAlt(localctx, 1)
                self.state = 243
                self.match(SPLParser.TRUE)
                pass
            elif token in [2]:
                self.enterOuterAlt(localctx, 2)
                self.state = 244
                self.match(SPLParser.FALSE)
                pass
            elif token in [15]:
                self.enterOuterAlt(localctx, 3)
                self.state = 245
                self.match(SPLParser.NUMBER)
                pass
            elif token in [16]:
                self.enterOuterAlt(localctx, 4)
                self.state = 246
                self.match(SPLParser.STRING)
                pass
            elif token in [11]:
                self.enterOuterAlt(localctx, 5)
                self.state = 247
                self.match(SPLParser.LPAREN)
                self.state = 248
                localctx.ex = self.expression()
                self.state = 249
                self.match(SPLParser.RPAREN)
                pass
            elif token in [17]:
                self.enterOuterAlt(localctx, 6)
                self.state = 251
                self.match(SPLParser.ID)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





